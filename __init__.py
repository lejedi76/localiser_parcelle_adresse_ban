#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
/***************************************************************************
        Aller sur une parcelle - Zoomer de la région à la parcelle
                             -------------------
    begin                : 10-10-2011
    copyright            : (C) 2011 par Mathieu Rajerison
    email                : mathieu.rajerison@developpement-durable.gouv.fr
 ***************************************************************************/
/*  Vous êtes libres d'utilsier les sources de ce script à condition de
/*  mentionner son auteur
/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
                                Evolutions
                         -------------------
 16/07/12 - Version 0.3 - Philippe DEBOEUFS - philippe.deboeufs@agriculture.gouv.fr - sauvegarde de la dernière région et du dernier département utilisés
 28/08/12 - Version 0.4 - Philippe DESBOEUFS - ajustement du plugin pour adaptation aux  versions de python < 2.7 et donc aux versionde QGIS < 1.8
 14/09/12 - Version 0.5 - Philippe DESBOEUFS - ajustement du plugin pour adaptation aux  Correction d'un bug provenant du tri d'une liste. Décalage dans les communes pointées.
 20/12/12 - Version 1.0 - Mathieu RAJERISON - Mise en conformité du plugin avec Cartélie 2.0
 29/05/13 - Version 1.5 - Frederic LASSERON - Ajout du geocodage adresse geocodeur.i2 1.0
 13/06/13 - Version 1.6 - Frederic LASSERON - Ajout croix localisation, tabulation , correction bug 1.5 pour geocodage adresse geocodeur.i2 1.0
 15/10/13 - Version 1.6 - Frederic LASSERON - Mise au format pour Qgis 2.0
 25/03/15 - Version 1.8 - Frederic LASSERON - Correctif pour version Qgis 2.6.1 et probleme ouverture urllib et test web service cartelie sur CRS pour affichage message erreur.
 18/05/16 - Version 2.0 - Frederic LASSERON - Mise en place acces full internet pour les web services et utilisation du proxy configuré dans Qgis, utilisation du CRS du projet 
 18/07/16 - Version 2.1 - Philippe DESBOEUFS - Agrandissement interface et adaptation pour les CRS non EPSG (ex: WGS84)
 21/07/16 - Version 2.2 - Frederic LASSERON - retrait accentuation metadata.txt pour fonctionner en 2.16 et encodage ANSI
 23/08/16 - Version 2.3 - Frederic LASSERON - correction retour encoding nom de commune accentuée pour geocodage BAN et activation bouton MinimizeWindow
 ***************************************************************************/
"""


def classFactory(iface):
    from localise import plugin
    return plugin(iface)
