#!/usr/bin/env python
# -*- coding: utf-8 -*-
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from PyQt4.QtNetwork import QNetworkProxy
from qgis.core import *
from qgis.gui import QgsVertexMarker

import urllib2,urllib,json,simplejson, os, string, time
from urllib2 import URLError, HTTPError
from xml.dom import minidom


from ui_control import ui_control

import resources

class plugin:

	def __init__(self, iface):
		self.iface = iface
		# Initialiser parametres de recherche
		import ConfigParser
		self.config = ConfigParser.RawConfigParser()
		self.config.add_section('recherche')
		# Stockage des marqueurs de croix recherche
		self.tmpGeometry = []
		
		## Position du fichier pour sauvegarder les paramètres (LocaliserParcelle.cfg) :
		# s'il y a un QGIS.ini, on le place à côté
		# sinon, dans USERPROFILE\.qgis2
		pluginPath= os.path.abspath(os.path.dirname(__file__)) + os.sep
		s = QSettings()
		iniFic= s.fileName()
		if QFile.exists(iniFic): #Si la config QGIS est stockee dans QGIS/QGIS.ini
			dossier= os.path.abspath(os.path.dirname(iniFic))+ os.sep
		else: #installation classique sous Windows (config dans base de registre)
			dossier= os.getenv('USERPROFILE') + os.sep +'.qgis2'+ os.sep
			rep= QDir(dossier)
			if not rep.exists(): rep.mkpath(dossier)
		self.configFile= dossier +"LocaliserParcelle.cfg"
		if not QFile.exists(self.configFile): # Copier le modele dans le profil utilisateur
			if QFile.exists(pluginPath + "recherche.cfg"):
				QFile.copy( pluginPath + "recherche.cfg", self.configFile )
		
		
	def doGetQgisProxy(self):
		settings = QSettings() #getting proxy from qgis options settings
		proxyEnabled = settings.value("proxy/proxyEnabled", "")
		proxyType = settings.value("proxy/proxyType", "" )
		proxyHost = settings.value("proxy/proxyHost", "" )
		proxyPort = settings.value("proxy/proxyPort", "" )
		proxyUser = settings.value("proxy/proxyUser", "" )
		proxyPassword = settings.value("proxy/proxyPassword", "" )
		return (proxyEnabled, proxyHost, proxyPort, proxyUser, proxyPassword, proxyType)

	def initGui(self):
		self.action = QAction(QIcon(":/plugins/localiser_parcelle_adresse/icone.png"), "Localiser Parcelle Adresse (Ban)", self.iface.mainWindow())
		self.iface.mainWindow().setAttribute(Qt.WA_DeleteOnClose)
		self.action.setWhatsThis("Aller de la region a la parcelle ou Adresse")
		self.action.setStatusTip("Aller de la region a la parcelle ou Adresse")
		QObject.connect(self.action, SIGNAL("activated()"), self.run)
		self.iface.addToolBarIcon(self.action)
		self.iface.addPluginToMenu("&Localiser Parcelle ou Adresse (Ban)", self.action)

	def unload(self):
		self.iface.removePluginMenu("&Localiser Parcelle ou Adresse (Ban)",self.action)
		self.iface.removeToolBarIcon(self.action)

	def close_Event(self, event):
		self.doEraseMark()

	def run(self):
		flags = Qt.WindowTitleHint | Qt.WindowSystemMenuHint | Qt.WindowMaximizeButtonHint  | Qt.WindowMinimizeButtonHint
		self.dlg = ui_control(self.iface.mainWindow(), flags)
		# fermeture de la fenetre du plugin on deroute sur une fonction interne
		self.dlg.closeEvent = self.close_Event
		##############################
		# VARIABLES D'INITIALISATION #
		##############################
		self.lstListes = [self.dlg.lRegion, self.dlg.lDepartement, self.dlg.lCommune, self.dlg.lSection, self.dlg.lParcelle]
		self.lstListes_label = ["-- REGION --", "-- DEPARTEMENT --", "-- COMMUNE --", "-- SECTION --", "-- PARCELLE --"]
		self.results = [1,2,3,4,5]
		########################
		#INSERTION DES SIGNAUX #
		########################
		QObject.connect(self.dlg.bInfo, SIGNAL('clicked()'),self.doDonnerInfos)
		QObject.connect(self.dlg,SIGNAL("lastWindowClosed()"),self.doClose)
		QObject.connect(self.dlg.bQuit, SIGNAL('clicked()'),self.doClose)
		QObject.connect(self.dlg.bErase, SIGNAL('clicked()'),self.doErase)
		QObject.connect(self.dlg.lRegion, SIGNAL('activated(int)'),self.doRemplirListeDepartements)
		QObject.connect(self.dlg.lDepartement, SIGNAL('activated(int)'),self.doRemplirListeCommunes)
		QObject.connect(self.dlg.lCommune, SIGNAL('activated(int)'),self.doRemplirListeSections)
		QObject.connect(self.dlg.lSection, SIGNAL('activated(int)'),self.doRemplirListeParcelles)
		QObject.connect(self.dlg.bZoom, SIGNAL('clicked()'),self.doLocaliser)
		QObject.connect(self.dlg.adrin, SIGNAL('returnPressed()'),self.doLocaliserAdresseBan)

		self.dlg.show()

		# Charger les paramètres de la dernière recherche depuis le fichier de config
		self.config.remove_section( 'REGIONS' )
		self.config.remove_section( 'DEPARTEMENTS' ) 
		self.config.remove_section( 'COMMUNES' )
		self.doRemplirListedApres(0)

		try: self.config.read( self.configFile ) #os.path.dirname(__file__)+'/recherche.cfg' )
		except:
			self.doRemplirListedApres(0)
			return
		#	Se positionner sur la scale selectionner lors de la derniere recherche :
		try:	scale = self.config.getint('recherche','scale')
		except:	return
		self.dlg.scale.setValue(scale)

		####################################
		# REMPLISSAGE DES LISTES		 #
		####################################
		### LISTE 0 = REGION
		try:	lesregions = self.config.items('REGIONS')
		except:
			self.doRemplirListedApres(0)
			return
		if len(lesregions)<20:
			self.doRemplirListedApres(0)
			return
		self.lstListes[0].clear(); self.lstListes[0].addItem(self.lstListes_label[0])
		li = []
		for R,nom in sorted(lesregions, key=self.val): #trier selon le nom de la region
			self.lstListes[0].addItem(nom)
			li.append( {'nom': nom, 'code': R.upper()} )
		self.results[0] = li
		#	Se positionner sur la region choisie lors de la derniere recherche :
		try:	region = self.config.getint('recherche','region')
		except:	return
		self.dlg.lRegion.setCurrentIndex( region+1 )

		### LISTE 1 = DEPARTEMENT
		try:	lesdep = self.config.items('DEPARTEMENTS')
		except:	return
		self.lstListes[1].clear(); self.lstListes[1].addItem(self.lstListes_label[1])
		li = []
		for D,nom in sorted(lesdep, key=self.val):
			self.lstListes[1].addItem(nom)
			li.append( {'nom': nom, 'code': D.upper()} )
		self.results[1] = li
		#	Se positionner sur la departement choisi lors de la derniere recherche :
		try:	dep = self.config.getint('recherche','dep')
		except:	return
		self.lstListes[1].setCurrentIndex( dep+1 )

		### LISTE 2 = COMMUNE
		try:	lescom = self.config.items('COMMUNES')
		except:	return
		self.lstListes[2].clear(); self.lstListes[2].addItem(self.lstListes_label[2])
		li = []
		for C,nom in sorted(lescom, key=self.val):
			self.lstListes[2].addItem(nom)
			li.append( {'nom': nom, 'code': C.upper()} )
		self.results[2] = li
		#	Se positionner sur la commune choisie lors de la derniere recherche :
		#try:	ville = self.config.getint('recherche','ville')
		#except:	return
		#self.lstListes[2].setCurrentIndex( ville+1 )
		if self.lstListes[2].currentIndex() >0:
			self.dlg.commune_adresse_enable(self.dlg.lCommune.currentText())
		elif self.lstListes[2].currentIndex() ==0:
			self.dlg.commune_adresse_disable('')

	def val(self, T): # fonction pour le tri des items de self.config selon les valeurs
		return T[1]
	
	def doErase(self):
		self.dlg.commune_adresse_disable('')
		for j in range(3, 5):
			self.lstListes[j].clear(); self.lstListes[j].addItem(self.lstListes_label[j])
		self.lstListes[2].setCurrentIndex(0)
		# Memoriser ces paramètres de recherche dans le fichier de config
		self.doEnregistrerParametres(self.config)
		self.doEraseMark()

	def doEraseMark(self):
		# detruire les precedents marqueurs
		for i in range(0,len(self.tmpGeometry)):
			self.iface.mapCanvas().scene().removeItem(self.tmpGeometry[i])
		self.tempGeometry = []

	def doClose(self):
		self.dlg.reject()
		self.doEraseMark()

	def doEnregistrerParametres(self, c):
		# Memoriser ces paramètres de recherche dans le fichier de config
		self.config.set('recherche', 'scale',self.dlg.scale.value())
		conf = open( self.configFile, 'w' )
		#conf = open(os.path.dirname(__file__)+'/recherche.cfg', 'w')
		c.write(conf)
		conf.close()

	def doRemplirListeDepartements(self, index):
		self.doRemplirListedApres(1)

	def doRemplirListeCommunes(self, index):
		self.doRemplirListedApres(2)
		
	def doRemplirListeSections(self, index):
		self.doRemplirListedApres(3)
		
	def doRemplirListeParcelles(self, index):
		self.doRemplirListedApres(4)
	#######################################	
	# REINITIALISATION DES LISTES D'APRES #
	#######################################
	def doRemplirListedApres(self, indexListe):
		for j in range(indexListe, 5):
			self.lstListes[j].clear(); self.lstListes[j].addItem(self.lstListes_label[j])

		#############################################
		# RECUPERATION des DONNEES SERVICE CARTELIE #
		#############################################
		if indexListe > 0:
			index = self.lstListes[indexListe-1].currentIndex()-1
			code = self.results[indexListe-1][index]["code"]
			url = 'http://cartelie.application.developpement-durable.gouv.fr/cartelie/localize?niveauBase=0&niveau='+str(indexListe)+'&projection=EPSG_2154&parent='+code
			#QMessageBox.information(self.iface.mainWindow(),"url"+str(indexListe), "index="+ str(index) +"		"+url)	 
		else:
			url = 'http://cartelie.application.developpement-durable.gouv.fr/cartelie/localize?niveauBase=0&niveau=0&projection=EPSG_2154'
		# recuperer le proxy de qgis pour une connexion internet
		(penabled, phost, pport, puser, ppassword, ptype) = self.doGetQgisProxy()
		print(url)
		req = urllib2.Request(url)
		req.set_proxy(str(phost)+':'+str(pport), 'http')
		rep = urllib2.urlopen(req)
		result = json.load(rep)
		result.sort(key=dict.values) # trier par valeurs pour assurer le tri alphabetique des noms
		self.results[indexListe] = result

		###################################
		# REMPLISSAGE DE LA LISTE D'APRES #
		###################################
		n = len(result)
		for i in range(n):
			if indexListe == 1:
					libelle = result[i]["nom"]+" ("+result[i]["code"]+")"
			else:
					libelle = result[i]["nom"]
			self.lstListes[indexListe].addItem(libelle)

		#######################################
		# Enregistrer parametres de recherche #
		#######################################
		# enregistre le choix de la région ou du dép.
		if indexListe==0:
			self.dlg.commune_adresse_disable('')
			self.config.remove_section('REGIONS')
			self.config.add_section('REGIONS')
			for i in range(n):
				self.config.set('REGIONS', result[i]["code"], result[i]["nom"])
		elif indexListe==1:
			self.dlg.commune_adresse_disable('')
			self.config.set('recherche', 'region', index)
			self.config.remove_option('recherche', 'dep')
			self.config.remove_section('DEPARTEMENTS')
			self.config.add_section('DEPARTEMENTS')
			for i in range(n):
				libelle = result[i]["nom"]+" ("+result[i]["code"]+")"
				self.config.set('DEPARTEMENTS', result[i]["code"], libelle)
		elif indexListe==2:
			self.dlg.commune_adresse_disable('')
			self.config.set('recherche', 'dep', index)
			self.config.remove_section('COMMUNES')
			self.config.add_section('COMMUNES')
			for i in range(n):
				self.config.set('COMMUNES', result[i]["code"], result[i]["nom"])
		elif indexListe==3:
			 # mise par defaut de la commune si indexliste >=3 à partir de section dans recherche adresse
			 self.dlg.commune_adresse_enable(self.dlg.lCommune.currentText())
			 #self.config.set('recherche', 'ville', index)
		## /Enregistrer parametres de recherche

	def doLocaliser(self):
		# Si tab Parcelle activer recherche parcelle
		if self.dlg.infracommune.currentIndex() == 0:
			self.doLocaliserParcelle()
		if self.dlg.infracommune.currentIndex() == 1:
			self.doLocaliserAdresseBan()

	def doLocaliserAdresseBan(self):
		# Memoriser ces paramètres de recherche dans le fichier de config
		self.doEnregistrerParametres(self.config)
		self.dlg.adrout.clear()
		a = self.dlg.adrin.text().encode('UTF-8')
		c = self.results[2][self.lstListes[2].currentIndex()-1]["code"]
		url = 'http://api-adresse.data.gouv.fr/search/?q=%s&citycode=%s&limit=1'  % (urllib.quote_plus(a) , urllib.quote_plus(c))
		# recuperer le proxy de qgis pour une connexion internet
		(penabled, phost, pport, puser, ppassword, ptype) = self.doGetQgisProxy()
		try:
			req = urllib2.Request(url)
			req.set_proxy(str(phost)+':'+str(pport), 'http')
			rep = urllib2.urlopen(req)
		except URLError, e:
			QMessageBox.warning(self.dlg,"Message :",unicode('(Erreur Url) - Serveur Geocodeur BAN Etalab injoignable, (réessayer plus tard ...)'+ str(e.code),'UTF-8'))
		except HTTPError, e:
			QMessageBox.warning(self.dlg,"Message :",unicode('(Erreur http) - Serveur Geocodeur BAN Etalab injoignable, (réessayer plus tard ...)'+ str(e.code),'UTF-8'))
		else:
			jsonResponse = json.load(rep)
			if len(jsonResponse['features']) > 0 :
				adresse = jsonResponse['features'][0]['properties']['label'].encode('UTF-8')
				x = float(jsonResponse['features'][0]['geometry']['coordinates'][0])
				y = float(jsonResponse['features'][0]['geometry']['coordinates'][1])
				score = round(float(jsonResponse['features'][0]['properties']['score'])*100,2)
				if jsonResponse['features'][0]['properties']['type'].lower()=='housenumber' :
					type="Plaque adresse"
				elif jsonResponse['features'][0]['properties']['type'].lower()=='street' :
					type="Voie"
				else: type=""
				#QMessageBox.warning(self.dlg,"Message :",'adresse %s !!' % str(adresse))
				self.dlg.affiche_adresse('<html><body><p align=left>Trouvé (%s):<br>%s<br>correspondance=%s &#37;</p></body></html>' % (str(type),str(adresse),str(score)))
				# Transformer les coordonnees du 4326 du Gécodeur Etalab -> Systeme projection projet
				# recuperation CRS projet
				coordTr= QgsCoordinateTransform(4326, self.iface.mapCanvas().mapRenderer().destinationCrs().toWkt())
				point= coordTr.transform( QgsPoint(x,y) )
				"""
				codeCRS=str(self.iface.mapCanvas().mapRenderer().destinationCrs().authid()).split(":")[1]
				f=QgsCoordinateReferenceSystem()
				f.createFromSrid(4326)
				t=QgsCoordinateReferenceSystem()
				t.createFromSrid(int(codeCRS))
				transformer = QgsCoordinateTransform(f,t)
				point = QgsPoint(x,y)
				point = transformer.transform(point)
				"""
				x = point[0]
				y = point[1]
				# Zoomer sur le point
				self.doZoomer(x, y, x, y)
			else:
				self.dlg.affiche_adresse('<html><body><p align=left color=red><b>Adresse non trouvée</p></b></body></html>')
	
	def doLocaliserParcelle(self):
		# Memoriser ces paramètres de recherche dans le fichier de config
		self.doEnregistrerParametres(self.config)
		typeCRS= str(self.iface.mapCanvas().mapRenderer().destinationCrs().authid()).split(":")[0]
		if typeCRS=='EPSG':
			codeCRS=str(self.iface.mapCanvas().mapRenderer().destinationCrs().authid()).split(":")[1]
		else: #rechercher les coordonnees en Lambert93 puis les convertir:
			codeCRS= '2154'
		###################################################
		# RECUPERATION DE LA LISTE ACTIVE ET DE SON INDEX #
		###################################################
		indexListe = None
		for i in range(4,-1,-1): 
			if self.lstListes[i].currentIndex() > 0 :
				# RECUPERATION DE L'ID DE LA LISTE
				indexListe = i
				break
		if indexListe == None:
			QMessageBox.information(self.iface.mainWindow(), "Zoom impossible",unicode("Pas si vite! Vous n'avez même pas choisi de région où aller!",'UTF-8'))
		else:
			index_lActuelle = self.lstListes[indexListe].currentIndex()-1
		###########################################
		# CONSTRUCTION DE LA REQUETE ET LANCEMENT #
		###########################################
			if indexListe > 0:
				index_lPrecedente = self.lstListes[indexListe-1].currentIndex()-1
				code = self.results[indexListe-1][index_lPrecedente]["code"]
				url = 'http://cartelie.application.developpement-durable.gouv.fr/cartelie/localize?niveauBase=0&niveau='+str(indexListe)+'&projection=EPSG_'+str(codeCRS)+'&parent='+str(code)
			else:
				url = 'http://cartelie.application.developpement-durable.gouv.fr/cartelie/localize?niveauBase=0&niveau=0&projection=EPSG_'+str(codeCRS)
			# recuperer le proxy de qgis pour une connexion internet
			(penabled, phost, pport, puser, ppassword, ptype) = self.doGetQgisProxy()
			try:
				req = urllib2.Request(url)
				req.set_proxy(str(phost)+':'+str(pport), 'http')
				rep = urllib2.urlopen(req)
				#print rep.read()
			except URLError, e:
				QMessageBox.warning(self.dlg,"Message :",unicode('(Erreur Url) - Serveur Cartelie injoignable, (réessayer plus tard ...)'+ str(e.code),'UTF-8'))
				return
			except HTTPError, e:
				QMessageBox.warning(self.dlg,"Message :",unicode('(Erreur http) - Serveur Cartelie injoignable, (réessayer plus tard ...)'+ str(e.code),'UTF-8'))
				return
			try:
				jsonResponse=json.load(rep)
				jsonResponse.sort(key=dict.values) # trier par valeurs pour assurer le tri alphabetique des noms
				xmin= jsonResponse[index_lActuelle]["xmin"]
				ymin= jsonResponse[index_lActuelle]["ymin"]
				xmax= jsonResponse[index_lActuelle]["xmax"]
				ymax= jsonResponse[index_lActuelle]["ymax"]
				if typeCRS<>'EPSG': #il faut reprojeter les coordonnees selon le CRS du projet:
					QMessageBox.warning(self.dlg,u"Erreur doLocaliserParcelle() :",u'Erreur lors du traitement de la reponse du serveur Cartelie 2: <br>' )
					coordTr= QgsCoordinateTransform(2154, self.iface.mapCanvas().mapRenderer().destinationCrs().toWkt())
					rect= coordTr.transform( QgsRectangle(xmin,ymin,xmax,ymax) )
					xmin= rect.xMinimum()
					ymin= rect.yMinimum()
					xmax= rect.xMaximum()
					ymax= rect.yMaximum()
				self.doZoomer( xmin, ymin, xmax, ymax )
			except Exception, e:
				QMessageBox.warning(self.dlg,u"Erreur doLocaliserParcelle() :",u'Erreur lors du traitement de la reponse du serveur Cartelie : <br>'+ str(e) )

	def doZoomer(self,x1,y1,x2,y2):
		# Zoomer sur rectangle englobant
		mc=self.iface.mapCanvas()
		# modif 2.2 gestion exception et mis a echelle 0
		try:
			if self.iface.mapCanvas().mapRenderer().destinationCrs().mapUnits()==0: #si unités en metres
				scale = self.dlg.scale.value()
			else:  
				scale = 0 #pas des metres donc on ne peut pas utiliser ce parametre.
		except Exception, e:
			scale=0
		rect = QgsRectangle(x1 -scale, y1 -scale, x2 + scale, y2  + scale)
		mc.setExtent(rect)
		# Afficher une croix
		self.doEraseMark()
		v=QgsVertexMarker(mc)
		v.setCenter(QgsPoint((x2+x1)/2,(y1+y2)/2))
		v.setColor(QColor(255,0,0))
		v.setIconSize(10)
		v.setIconType(QgsVertexMarker.ICON_X) # ICON_BOX ICON_CROSS, ICON_X
		v.setPenWidth(2)
		self.tmpGeometry.append(v)
		mc.refresh()
		
	def doDonnerInfos(self):
		info = "Ce plugin exploite (en mode http):\n1) le web service du Ministère de l'Ecologie (Cartelie) de géolocalisation sur plusieurs échelles administratives (Région, Département, Commune, Section, Parcelle),\n2) le web service de géolocalisation adresse Etalab.gouv.fr-BAN.\n\nCes deux web services fonctionnent depuis des postes de travail ayant un accès internet, en utilisant le proxy configuré dans qgis en mode http et le système de projection courant du projet.\n\nVersion 2.3"
		QMessageBox.information(self.iface.mainWindow(),"A propos", unicode(info,'UTF-8'))
