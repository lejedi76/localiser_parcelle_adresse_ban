﻿# -*- coding: utf-8 -*-
from PyQt4.QtCore import *
from PyQt4.QtGui import *

from ui_localise import Ui_Dialog


class ui_control(QDialog, Ui_Dialog):
	def __init__(self, parent, fl):
		QDialog.__init__(self, parent, fl)
		self.setupUi(self)
	def commune_adresse_disable(self, output):
		self.adrin.clear()
		self.adrout.clear()
		self.infracommune.setCurrentIndex(0)
		self.infracommune.setEnabled(False)
	def commune_adresse_enable(self, output):
		self.adrout.clear()
		self.infracommune.setEnabled(True)
	def affiche_adresse(self, output):
		self.adrout.setEnabled(True)
		self.adrout.setText(unicode(output,'UTF-8'))
	def efface_adresse(self, output):
		self.adrout.clear()
